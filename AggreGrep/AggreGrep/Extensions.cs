using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace AggreGrep
{
    public static class Extensions
    {
        public static string EscapeMostRegexMetaCharacters(this String str)
        {
            return str.Replace("\\", "\\\\")
                      .Replace(".", "\\.")
                      .Replace("*", ".*") //allows for * to be wildcard, but eliminates possibility of searching for literal star
                      .Replace("+", "\\+")
                      .Replace("?", "\\?")
                      .Replace("|", "\\|")
                      .Replace("{", "\\{")
                      .Replace("[", "\\[")
                      .Replace("(", "\\(")
                      .Replace(")", "\\)")
                      .Replace("^", "\\^")
                      .Replace("$", "\\$")
                      .Replace("#", "\\#");
        }

        public static string UnescapeMostRegexMetaCharacters(this String str)
        {
            return str.Replace("\\#", "#")
                      .Replace("\\$", "$")
                      .Replace("\\^", "^")
                      .Replace("\\)", ")")
                      .Replace("\\(", "(")
                      .Replace("\\[", "[")
                      .Replace("\\{", "{")
                      .Replace("\\|", "|")
                      .Replace("\\?", "?")
                      .Replace("\\+", "+")
                      .Replace(".*", "*")
                      .Replace("\\.", ".")
                      .Replace("\\\\", "\\");
        }

        public static DataTable ToDataTable<T>(this IEnumerable<T> self)
        {
            var properties = typeof(T).GetProperties();

            var dataTable = new DataTable();
            foreach (var info in properties)
                dataTable.Columns.Add(info.Name, Nullable.GetUnderlyingType(info.PropertyType)
                   ?? info.PropertyType);

            foreach (var entity in self)
                dataTable.Rows.Add(properties.Select(p => p.GetValue(entity)).ToArray());

            return dataTable;
        }
        public static string ToCsv(this DataTable dataTable)
        {
            StringBuilder sbData = new StringBuilder();

            // Only return Null if there is no structure.
            if (dataTable.Columns.Count == 0)
                return null;

            foreach (var col in dataTable.Columns)
            {
                if (col == null)
                    sbData.Append(",");
                else
                    sbData.Append("\"" + col.ToString().Replace("\"", "\"\"") + "\",");
            }

            sbData.Replace(",", System.Environment.NewLine, sbData.Length - 1, 1);

            foreach (DataRow dr in dataTable.Rows)
            {
                foreach (var column in dr.ItemArray)
                {
                    if (column == null)
                        sbData.Append(",");
                    else
                        sbData.Append("\"" + column.ToString().Replace("\"", "\"\"") + "\",");
                }
                sbData.Replace(",", System.Environment.NewLine, sbData.Length - 1, 1);
            }

            return sbData.ToString();
        }
        public static bool In<T>(this T o, params T[] values)
        {
            if (values == null) return false;

            return values.Contains(o);
        }
        public static bool In<T>(this T o, IEnumerable<T> values)
        {
            if (values == null) return false;

            return values.Contains(o);
        }

        public static IEnumerable<TSource> DistinctBy<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
        {
            HashSet<TKey> seenKeys = new HashSet<TKey>();
            foreach (TSource element in source)
            {
                if (seenKeys.Add(keySelector(element)))
                {
                    yield return element;
                }
            }
        }
    }
}
