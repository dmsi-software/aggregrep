﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;

namespace AggreGrep
{
    class Program
    {
        static void Main(string[] args)
        {
            string version = "devel";
            string rootDirectory = "K:\\dmsi\\";
            string saveFileDirectory;
            string[] paths;
            string searchPattern;
            string fileMask = "*.*";
            string errorMessage = "";
            string[] subDirectoriesToSearch = new string[5] { "srcw",
                                                              "srcp",
                                                              "srci",
                                                              "src",
                                                              "trg"};
            Program p = new Program();

            if (args.Length.In(3, 4, 5))
            {
                saveFileDirectory = args[0];
                version = args[1];
                searchPattern = args[2].EscapeMostRegexMetaCharacters();
                if (args.Length >= 4)
                {
                    subDirectoriesToSearch = args[3].Split(',');
                    for(int i = 0; i < subDirectoriesToSearch.Length; i++)
                    {
                        subDirectoriesToSearch[i] = subDirectoriesToSearch[i].TrimStart('\\').TrimEnd('\\');
                    }
                }
                if (args.Length >= 5)
                {
                    fileMask = args[4];
                }
            }
            else if (Debugger.IsAttached)
            {
                saveFileDirectory = "c:\\scripts\\";
                version = "541";
                searchPattern = "Ten.,Fourth,etl_log";
                fileMask = "*.*";
            }
            else
            {
                Console.WriteLine("Failed to pass valid parameter set.  Should be CSVSaveFileDirectory, VersionNum, SearchPattern, SubDirectories (optional), and FileMask (optional).");
                return;
            }

            if (p.ValidParameters(saveFileDirectory, 
                                  version, 
                                  searchPattern, 
                                  fileMask, 
                                  rootDirectory,
                                  subDirectoriesToSearch, 
                                  out paths, 
                                  out errorMessage))
            {
                p.RunTasks(version, 
                           saveFileDirectory, 
                           paths, 
                           searchPattern, 
                           fileMask);
                Console.WriteLine("Done!");
            }
            else
            {
                Console.WriteLine(errorMessage);
            }
        }

        private bool ValidParameters(string saveFileDirectory,
                                     string version,
                                     string searchPattern,
                                     string fileMask,
                                     string rootDirectory,
                                     string[] subdirsToSearch,
                                     out string[] paths,
                                     out string errorMessage)
        {
            bool validParams = true;
            int versionParsed = -1;
            paths = null;

            errorMessage = "";

            if (!Directory.Exists(saveFileDirectory))
            {
                validParams = false;
                errorMessage += $"Directory {saveFileDirectory} does not exist.  Please enter a valid save file directory." + Environment.NewLine;
            }
            if (!((int.TryParse(version, out versionParsed) && version.ToString().Length == 3) || version.ToLower() == "devel"))
            {
                validParams = false;
                errorMessage += $"{version} is not a valid version number. Please enter a valid version number or use 'devel'." + Environment.NewLine;
            }
            if (searchPattern.Trim() == "")
            {
                validParams = false;
                errorMessage += $"Search pattern empty.  Please enter a search pattern." + Environment.NewLine;
            }
            if (validParams)
            {
                string versionChar = "";

                if (int.TryParse(version, out versionParsed))
                    versionChar = "v";

                if (Array.IndexOf(subdirsToSearch, "src") >= 0)
                {
                    subdirsToSearch = SplitSrcDir(rootDirectory + versionChar + version, subdirsToSearch);

                }

                paths = new string[subdirsToSearch.Length];

                for (int i = 0; i < paths.Length; i++)
                {
                    paths[i] = rootDirectory + versionChar + version + "\\" + subdirsToSearch[i] + "\\";
                }
            }

            return validParams;
        }

        private string[] SplitSrcDir(string dirToSearch,
                                     string[] subDirsToSearch)
        {
            List<string> newDirs = new List<string>();
            DirectoryInfo di = new DirectoryInfo(dirToSearch + "\\src");

            foreach (string subDir in subDirsToSearch)
            {
                if (subDir != "src")
                {
                    newDirs.Add(subDir);
                } 
            }

            foreach (DirectoryInfo dir in di.GetDirectories())
            {
                newDirs.Add("src\\" + dir.Name);
            }


            return newDirs.ToArray();
        }

        private void RunTasks(string version,
                              string ipcSaveFileDirectory,
                              string[] ipcPaths,
                              string ipcSearchPatterns,
                              string ipcSearchMask)
        {
            ConcurrentBag<FSGrep.Result> bag = new ConcurrentBag<FSGrep.Result>();
            IEnumerable<SummaryResults> results = new List<SummaryResults>();
            string nowString = DateTime.Now.ToString("yyyyMMddHHmm");
            string[] searchStrings = ipcSearchPatterns.Split(',');
            bool debugTasks = false;


            if (Debugger.IsAttached && debugTasks)
            {
                //Debug code because walking through parallel tasks is ANNOYING!
                foreach (string path in ipcPaths)
                {
                    foreach (string searchString in searchStrings)
                    {
                        foreach (FSGrep.Result r in FindResults(path,
                                                            ipcSearchPatterns,
                                                            ipcSearchMask))
                        {
                            bag.Add(r);
                        }
                    }
                };
            }
            else
            {
                //Faster parallel processing
                Parallel.ForEach(ipcPaths, (path) =>
                {
                    Parallel.ForEach(searchStrings, (searchString) =>
                    {
                        foreach (FSGrep.Result r in FindResults(path,
                                                                searchString,
                                                                ipcSearchMask))
                        {
                            bag.Add(r);
                        }
                    });
                });
            }

            results = AggregateResults(ipcSearchPatterns, bag);

            File.WriteAllText(ipcSaveFileDirectory + $"\\AggregateResults_{version}_{nowString}.csv", results.ToDataTable<SummaryResults>().ToCsv());
            File.WriteAllText(ipcSaveFileDirectory + $"\\RawResults_{version}_{nowString}.csv", bag.ToDataTable<FSGrep.Result>().ToCsv());
        }
        private IEnumerable<FSGrep.Result> FindResults(string ipcRootpath,
                                                       string ipcSearchPattern,
                                                       string ipcSearchMask = "*.*")
        {
            FSGrep versionGrep = new FSGrep
            {
                RootPath = ipcRootpath,
                FileSearchLinePattern = ipcSearchPattern,
                FileSearchMask = ipcSearchMask
            };

            return versionGrep.GetMatchingFiles();
        }

        private IEnumerable<SummaryResults> AggregateResults(string ipcSearchPatterns,
                                                             ConcurrentBag<FSGrep.Result> results)
        {
            string[] searchPatterns = ipcSearchPatterns.Split(',');

            foreach (string pattern in searchPatterns)
            {
                yield return new SummaryResults(pattern.UnescapeMostRegexMetaCharacters(), results);
            }
        }
    }
}
