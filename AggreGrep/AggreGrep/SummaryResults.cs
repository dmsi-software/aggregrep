﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AggreGrep
{
    public class SummaryResults
    {
        public string _TechDebtMeasurement { get; set; }
        public string Results { get; set; }

        public SummaryResults(string searchPattern, 
                              ConcurrentBag<FSGrep.Result> rawGrepResults)
        {
            _TechDebtMeasurement = searchPattern;
            Results = ResultsString(rawGrepResults);
        }

        private string ResultsString(ConcurrentBag<FSGrep.Result> results)
        {
            int numInstances = 0;
            int numFiles = 0;

            numInstances = results.Where(i => i.SearchPattern == _TechDebtMeasurement).Count(); 
            numFiles = results.Where(i => i.SearchPattern == _TechDebtMeasurement).DistinctBy(i => i.FilePath).Count();

            return $"{numInstances} time{(numInstances == 1 ? "" : "s")}, {numFiles} file{(numFiles == 1 ? "" : "s")}";
        }
    }
}
