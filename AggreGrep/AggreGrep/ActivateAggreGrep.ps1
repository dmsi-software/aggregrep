﻿param
(
    [Parameter(Mandatory=$true)]
    [ValidatePattern('(\d{3})$')]
    [string]
    $version
)

#variable declaration
$searchPatterns = "RUN costcnv4.p,RUN costcnv5.p,RUN uomconv.p,RUN uomconv1.p,RUN uomconv2.p,RUN p_uomconv2,RUN p_qtycnv,f_uomcode,RUN p_uomconv3,RUN uomcode.p,RUN p_uomcode,qtyuomproc.i,RUN getvalue2.p,{ttdyndata.i},generic_error,RECID*(,ROWID*(,FIELD*(*)*WHERE"
$subdirectoriesToSearch = "srcw,srcp,srci,src,trg"

$KDrive = "\\dmsinet\omaha\devel"
$executableDir = "$KDrive\DMSI\AggreGrep\"
$executableName = "AggreGrep.exe"
$saveDir        = "$executableDir\Results\"
$oldverbose = $VerbosePreference
$VerbosePreference = "continue"

if (Get-PSDrive aggregrep  -ErrorAction SilentlyContinue) 
{
    #unmap orphaned drive if in use so we can use it
    Write-Verbose "Unmap aggregrep Drive"
    Remove-PSDrive aggregrep
}

#map aggregrep drive to run command line application
Write-Verbose "Map aggregrep drive to run executable"
New-PSDrive -Name aggregrep -PSProvider FileSystem -Root $KDrive

Set-Location $executableDir

Write-Verbose "Filepath: $executableDir$executableName"
Write-Verbose "ArgumentList: $saveDir $version ""$searchPatterns"" ""$subdirectoriesToSearch"""

Start-Process -FilePath $executableDir$executableName -ArgumentList "$saveDir $version ""$searchPatterns"" ""$subdirectoriesToSearch""" -Wait


if (Get-PSDrive aggregrep  -ErrorAction SilentlyContinue) 
{
    #unmap aggregrep drive
    Write-Verbose "Unmap aggregrep Drive"
    Remove-PSDrive aggregrep
}

$VerbosePreference = $oldverbose

Set-Location C:\